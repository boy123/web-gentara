<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Services_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'services/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'services/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'services/index.html';
            $config['first_url'] = base_url() . 'services/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Services_model->total_rows($q);
        $services = $this->Services_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'services_data' => $services,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'services/services_list',
            'konten' => 'services/services_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Services_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_services' => $row->id_services,
		'judul' => $row->judul,
		'icon' => $row->icon,
		'deskripsi' => $row->deskripsi,
	    );
            $this->load->view('services/services_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('services'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'services/services_form',
            'konten' => 'services/services_form',
            'button' => 'Create',
            'action' => site_url('services/create_action'),
	    'id_services' => set_value('id_services'),
	    'judul' => set_value('judul'),
	    'icon' => set_value('icon'),
	    'deskripsi' => set_value('deskripsi'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $icon = upload_gambar_biasa('icon', 'image/service/', 'jpg|png|jpeg', 10000, 'icon');
            if (strpos($icon, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($icon,'error'));
                redirect('services/create','refresh');
            }

            $data = array(
		'judul' => $this->input->post('judul',TRUE),
		'icon' => $icon,
		'deskripsi' => $this->input->post('deskripsi',TRUE),
	    );

            $this->Services_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('services'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Services_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'services/services_form',
                'konten' => 'services/services_form',
                'button' => 'Update',
                'action' => site_url('services/update_action'),
		'id_services' => set_value('id_services', $row->id_services),
		'judul' => set_value('judul', $row->judul),
		'icon' => set_value('icon', $row->icon),
		'deskripsi' => set_value('deskripsi', $row->deskripsi),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('services'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_services', TRUE));
        } else {

            $retVal = ($_FILES['icon']['name'] == '') ? $_POST['icon_old'] : upload_gambar_biasa('icon', 'image/service/', 'jpg|png|jpeg', 10000, 'icon');
            if (strpos($retVal, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($retVal,'error'));
                redirect('services/update/'.$this->input->post('id_services', TRUE),'refresh');
            }

            $data = array(
		'judul' => $this->input->post('judul',TRUE),
		'icon' => $retVal,
		'deskripsi' => $this->input->post('deskripsi',TRUE),
	    );

            $this->Services_model->update($this->input->post('id_services', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('services'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Services_model->get_by_id($id);

        if ($row) {
            $this->Services_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('services'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('services'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

	$this->form_validation->set_rules('id_services', 'id_services', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-02 14:01:42 */
/* https://jualkoding.com */