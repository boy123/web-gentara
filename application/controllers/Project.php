<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'project/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'project/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'project/index.html';
            $config['first_url'] = base_url() . 'project/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Project_model->total_rows($q);
        $project = $this->Project_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'project_data' => $project,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'project/project_list',
            'konten' => 'project/project_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Project_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_project' => $row->id_project,
		'judul' => $row->judul,
		'isi' => $row->isi,
		'gambar' => $row->gambar,
	    );
            $this->load->view('project/project_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'project/project_form',
            'konten' => 'project/project_form',
            'button' => 'Create',
            'action' => site_url('project/create_action'),
	    'id_project' => set_value('id_project'),
	    'judul' => set_value('judul'),
	    'isi' => set_value('isi'),
	    'gambar' => set_value('gambar'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            
            $gambar = upload_gambar_biasa('gambar', 'image/project/', 'jpg|png|jpeg', 10000, 'gambar');
            if (strpos($gambar, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($gambar,'error'));
                redirect('project/create','refresh');
            }

            $data = array(
		'judul' => $this->input->post('judul',TRUE),
		'isi' => $this->input->post('isi',TRUE),
		'gambar' => $gambar,
	    );

            $this->Project_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('project'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Project_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'project/project_form',
                'konten' => 'project/project_form',
                'button' => 'Update',
                'action' => site_url('project/update_action'),
		'id_project' => set_value('id_project', $row->id_project),
		'judul' => set_value('judul', $row->judul),
		'isi' => set_value('isi', $row->isi),
		'gambar' => set_value('gambar', $row->gambar),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_project', TRUE));
        } else {

            $retVal = ($_FILES['gambar']['name'] == '') ? $_POST['gambar_old'] : upload_gambar_biasa('gambar', 'image/project/', 'jpg|png|jpeg', 10000, 'gambar');
            if (strpos($retVal, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($retVal,'error'));
                redirect('project/update/'.$this->input->post('id_project', TRUE),'refresh');
            }

            $data = array(
		'judul' => $this->input->post('judul',TRUE),
		'isi' => $this->input->post('isi',TRUE),
		'gambar' => $retVal,
	    );

            $this->Project_model->update($this->input->post('id_project', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('project'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Project_model->get_by_id($id);

        if ($row) {
            $this->Project_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('project'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('isi', 'isi', 'trim|required');

	$this->form_validation->set_rules('id_project', 'id_project', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Project.php */
/* Location: ./application/controllers/Project.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-02 14:01:50 */
/* https://jualkoding.com */