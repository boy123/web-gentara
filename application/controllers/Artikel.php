<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artikel extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Artikel_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'artikel/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'artikel/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'artikel/index.html';
            $config['first_url'] = base_url() . 'artikel/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Artikel_model->total_rows($q);
        $artikel = $this->Artikel_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'artikel_data' => $artikel,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Artikel',
            'konten' => 'artikel/artikel_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Artikel_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_artikel' => $row->id_artikel,
		'slug' => $row->slug,
		'id_kategori' => $row->id_kategori,
		'judul' => $row->judul,
		'isi' => $row->isi,
		'keyword' => $row->keyword,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
		'id_user' => $row->id_user,
	    );
            $this->load->view('artikel/artikel_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('artikel'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Artikel',
            'konten' => 'artikel/artikel_form',
            'button' => 'Create',
            'action' => site_url('artikel/create_action'),
	    'id_artikel' => set_value('id_artikel'),
	    'slug' => set_value('slug'),
	    'id_kategori' => set_value('id_kategori'),
	    'judul' => set_value('judul'),
	    'isi' => set_value('isi'),
	    'keyword' => set_value('keyword'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	    'id_user' => set_value('id_user'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $data = array(
		'slug' => url_title($this->input->post('judul'), 'dash', true),
		'id_kategori' => $this->input->post('id_kategori',TRUE),
		'judul' => $this->input->post('judul',TRUE),
		'isi' => $this->input->post('isi',TRUE),
		'keyword' => $this->input->post('keyword',TRUE),
		'created_at' => get_waktu(),
		'id_user' => $this->session->userdata('id_user'),
	    );

            $this->Artikel_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('artikel'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Artikel_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Artikel',
                'konten' => 'artikel/artikel_form',
                'button' => 'Update',
                'action' => site_url('artikel/update_action'),
		'id_artikel' => set_value('id_artikel', $row->id_artikel),
		'slug' => set_value('slug', $row->slug),
		'id_kategori' => set_value('id_kategori', $row->id_kategori),
		'judul' => set_value('judul', $row->judul),
		'isi' => set_value('isi', $row->isi),
		'keyword' => set_value('keyword', $row->keyword),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
		'id_user' => set_value('id_user', $row->id_user),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('artikel'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_artikel', TRUE));
        } else {
            $data = array(
		'slug' => url_title($this->input->post('judul'), 'dash', true),
		'id_kategori' => $this->input->post('id_kategori',TRUE),
		'judul' => $this->input->post('judul',TRUE),
		'isi' => $this->input->post('isi',TRUE),
		'keyword' => $this->input->post('keyword',TRUE),
		'updated_at' => get_waktu(),
		'id_user' => $this->session->userdata('id_user'),
	    );

            $this->Artikel_model->update($this->input->post('id_artikel', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('artikel'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Artikel_model->get_by_id($id);

        if ($row) {
            $this->Artikel_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('artikel'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('artikel'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
	$this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('isi', 'isi', 'trim|required');
	$this->form_validation->set_rules('keyword', 'keyword', 'trim|required');

	$this->form_validation->set_rules('id_artikel', 'id_artikel', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Artikel.php */
/* Location: ./application/controllers/Artikel.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-02 14:07:09 */
/* https://jualkoding.com */