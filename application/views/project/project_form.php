
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
	    <div class="form-group">
            <label for="isi">Isi <?php echo form_error('isi') ?></label>
            <textarea class="form-control editor" rows="3" name="isi" id="isi" placeholder="Isi"><?php echo $isi; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Gambar</label>
            <input type="file" class="form-control" name="gambar" />
            <input type="hidden" name="gambar_old" value="<?php echo $gambar ?>">
            <p>
                <?php if ($gambar != ''): ?>
                    *) Gambar sebelumnya <br>
                    <img src="image/project/<?php echo $gambar ?>" style="width: 100px;">
                <?php endif ?>
                
            </p>
        </div>
	    <input type="hidden" name="id_project" value="<?php echo $id_project; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('project') ?>" class="btn btn-default">Cancel</a>
	</form>
   