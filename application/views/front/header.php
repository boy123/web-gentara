<header id="header">
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
              <div class="info-box">
                <i class="fa fa-envelope"></i>
                <div class="info-text">
                  <h5>Email Address:</h5>
                  <a href="#"
                    ><span
                      class="__cf_email__"
                      data-cfemail="b4d7dbd2d1d1f4d0ddd5d3c6d5d99ad7dbd9"
                      >admin@gentara.co.id</span
                    ></a
                  >
                </div>
              </div>
              <div class="info-box">
                <i class="fa fa-phone"></i>
                <div class="info-text">
                  <h5>Contact Number:</h5>
                  <span>0852-7322-6780</span>
                </div>
              </div>
              <div class="info-box">
                <i class="fa fa-clock-o"></i>
                <div class="info-text">
                  <h5>Opening Time:</h5>
                  <a href="#">Senin-Sabtu: 9:00 - 17:30</a>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="top-social pull-right">
                <ul class="list-inline">
                  <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-linkedin-square"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-facebook-square"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-skype"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-pinterest-square"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="main-menu">
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="navbar-header">
              <button
                type="button"
                class="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#navbar-collapse"
              >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href=""
                ><img class="img-responsive" src="image/logo.png" alt="Logo"
              /></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li <?php echo $retVal = ($this->uri->segment(2) == '') ? 'class="active"' : '' ; ?>><a href="web">Home</a></li>
                <li <?php echo $retVal = ($this->uri->segment(2) == 'about_us') ? 'class="active"' : '' ; ?>><a href="web/about_us">About us</a></li>
                <li <?php echo $retVal = ($this->uri->segment(2) == 'services') ? 'class="active"' : '' ; ?>><a href="web/services">Services</a></li>
                <li <?php echo $retVal = ($this->uri->segment(2) == 'project') ? 'class="active"' : '' ; ?>><a href="web/project">Projects</a></li>
                <li <?php echo $retVal = ($this->uri->segment(2) == 'blog') ? 'class="active"' : '' ; ?>><a href="web/blog">Blog</a></li>
                <li <?php echo $retVal = ($this->uri->segment(2) == 'contact_us') ? 'class="active"' : '' ; ?>><a href="web/contact_us">Contact Us</a></li>
                
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </header>