<footer class="footer">
      <div class="contact-section">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <div class="contact-widget">
                <img class="img-responsive" src="image/logo.png" alt="" />
                <p>
                  Menjadi Konsultan Tambang Berwawasan Lingkungan Nomor 1 di Indonesia.
                </p>
                <p>
                  <li>Mengutamakan Integritas, Mutu, dan Kepuasan Pelanggan</li>
                  <li>Memberikan layanan terbaik kepada pengguna jasa</li>
                  <li>Pengembangan Skill dan Karakter Tim secara massive  dan berkelanjutan</li>
                  <li>Mengedepankan Perlindungan lingkungan</li>
                  <li>Memberikan yang terbaik kepada seluruh pemangku kepentingan</li>
                  

                </p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="contact-widget">
                <h3>Business Hours</h3>
                <p>
                  Our support available to help you 24 hours a day, seven days a
                  week.
                </p>
                <p>Monday - Thursday @ 09.00 - 17.30</p>
                <p>Friday & Saturday @ 10.00 - 16.00</p>
                <p>Sunday - <span> Closed </span></p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="contact-widget">
                <h3>Quick Links</h3>
                <ul>
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Projects</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">Latest News</a></li>
                  <li><a href="#">Shop</a></li>
                </ul>
                <ul>
                  <li><a href="#">Who We Are</a></li>
                  <li><a href="#">Creer</a></li>
                  <li><a href="#">Contac Us</a></li>
                  <li><a href="#">Features</a></li>
                  <li><a href="#">FAQ</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="contact-widget">
                <h3>Our Office</h3>
                <address>
                  <ul>
                    <li>
                      <span class="address">Address:</span>Grand Slipi Tower Level 42 Unit G-H Jl. S. Parman Kav.22-24, RT 001/ RW
004, Kelurahan Palmerah, Kecamatan Pal Merah, Jakarta Barat.

                    </li>

                    <li><span>Phone: </span>+62 852-7322-6780</li>
                    <li><span>Wa: </span>+62 811-1231-827</li>
                    <li>
                      <span>Email: </span
                      ><a href="#"
                        ><span
                          class="__cf_email__"
                          data-cfemail="0c7f797c7c637e784c68656d6b7e6d61226f6361"
                          >admin@gentara.co.id</span
                        ></a
                      >
                    </li>
                  </ul>
                </address>
                <ul class="footer-social list-inline">
                  <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-linkedin-square"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-facebook-square"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-pinterest-square"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="copyright-text text-center">
            <p>
              &copy; PT Gentala Bumi Nusantara<?php echo date('Y') ?> | Design & Developed By
              <a href="http://www.gentara.co.id/">IT Gentara</a>
            </p>
          </div>
        </div>
      </div>
    </footer>

    <script src="front/js/jquery.min.js"></script>
    <script src="front/js/modernizr.min.js"></script>
    <script src="front/js/bootstrap.min.js"></script>
    <script src="front/js/owl.carousel.min.js"></script>
    <script src="front/js/cubeportfolio.min.js"></script>
    <script src="front/js/magnific-popup.min.js"></script>
    <script src="front/js/jquery.backstretch.min.js"></script>
    <script type="text/javascript">
      jQuery(function ($) {

        'use strict';
        
        // -------------------------------------------------------------
        //  Background Slider Image Source
        // -------------------------------------------------------------

          

     
      
        // -------------------------------------------------------------
        //  Owl Carousel
        // -------------------------------------------------------------

        (function() {

            $("#recent-projects").owlCarousel({
                items:4,
          responsive:{
            0:{
              items:1
            },
            600:{
              items:2
            },
            1000:{
              items:4
            }
          },
                nav:true,
                autoplay:true,
                dots:false,
                nav:true,
                navText: [
                  "<i class='fa fa-angle-left'></i>",
                  "<i class='fa fa-angle-right'></i>"
                ],            
           
            });

        }()); 

        (function() {

            $(".testimonial-slider, #product-carousel").owlCarousel({
                items:1,
                nav:false,
                autoplay:true, 
          dots:true,
            });

        }());
      
      (function() {

            $("#brand-carousel").owlCarousel({
                items:6,
          responsive:{
            0:{
              items:3,
            },
            600:{
              items:4,
            },
            1000:{
              items:6,
            }
          },
                nav:false,
                autoplay:true,
            });

        }());

        // -------------------------------------------------------------
        //  Cubeportfolio
        // -------------------------------------------------------------

        
        (function () {
            
            $('#projects').cubeportfolio({
                filters: '#project-menu',
                animationType: 'sequentially',
                gapVertical: 30,
                gapHorizontal: 30, 

              });

        }()); 

        
      // -------------------------------------------------------------
        // Progress Bar
        // -------------------------------------------------------------
     
        $('.skill').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $.each($('div.progress-bar'),function(){
                    $(this).css('width', $(this).attr('aria-valuenow')+'%');
                });
                $(this).unbind('inview');
            }
        });  

        // -------------------------------------------------------------
        //  Magnific Popup
        // -------------------------------------------------------------

        (function() {

            $('.image-link').magnificPopup({
              type: 'image',
              gallery:{
                enabled:true,
              }
            });

        }());   

      
      
      
      
      
    // script end
    });

  
  
  
  


    </script>