<div class="breadcrumb-section image-bg">
      <div class="overlay"></div>
      <div class="breadcrumb-content container">
        <h1>Our Projects</h1>
        <ol class="breadcrumb">
          <li><a href="">Home</a></li>
          <li class="active">Project</li>
        </ol>
      </div>
    </div>
    <div class="recent-projects section-padding">
      <div class="container">
        <div class="section-title text-center">
          <div class="container">
            <h1>Our Projects</h1>
            <h2>We already completed</h2>
          </div>
        </div>
        <div class="row">

          <div id="projects">

            <?php foreach ($projects->result() as $project): ?>
              
            
            <div class="cbp-item project-content">
              <div class="project-title">
                <h3><a href="web/project_detail/<?php echo $project->id_project ?>"><?php echo $project->judul ?></a></h3>
                <span></span>
              </div>
              <div class="project-item">
                <a href="image/project/<?php echo $project->gambar ?>" class="image-link"
                  ><img
                    class="img-responsive"
                    src="image/project/<?php echo $project->gambar ?>"
                    alt=""
                /></a>
              </div>

              <div class="project-info">
                <p>
                  
                </p>
              </div>
              
            </div>
            <?php endforeach ?>

          </div>


        </div>
      </div>
    </div>