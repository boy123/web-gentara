<?php 
$this->db->where('id_project', $this->uri->segment(3));
$dt = $this->db->get('project')->row();
 ?>
 <div class="breadcrumb-section image-bg">
      <div class="overlay"></div>
      <div class="breadcrumb-content container">
        <h1>Projects Details</h1>
        <ol class="breadcrumb">
          <li><a href="">Home</a></li>
          <li class="active">Project Details</li>
        </ol>
      </div>
    </div>
    <div class="details-section section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-12">
            <div id="product-carousel">
              <div class="carousel-image">
                <img
                  class="img-responsive"
                  src="image/project/<?php echo $dt->gambar ?>"
                  alt="Image"
                />
              </div>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="details-info">
              <h2><?php echo $dt->judul ?></h2>
              <?php echo $dt->isi ?>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="recent-projects section-padding">
      <div class="container">
        <div class="section-title text-center">
          <div class="container">
            <h1>Other Projects</h1>
            <h2>Completed Recently</h2>
          </div>
        </div>
        <div class="row">

          <?php
          $sql = "SELECT * from project order by RAND() LIMIT 3";
           foreach ($this->db->query($sql)->result() as $rw): ?>

          <div class="col-sm-6 col-md-4">
            <div class="project-content">
              <div class="project-title">
                <h3><a href="web/project_detail/<?php echo $rw->id_project ?>"><?php echo $rw->judul ?></a></h3>
                <span></span>
              </div>
              <div class="project-item">
                <a href="image/project/<?php echo $rw->gambar ?>" class="image-link"
                  ><img
                    class="img-responsive"
                    src="image/project/<?php echo $rw->gambar ?>"
                    alt=""
                /></a>
              </div>
              <div class="project-info">
                <p>
                 
                </p>
              </div>
            </div>
          </div>

          <?php endforeach ?>
          
        </div>
      </div>
    </div> -->