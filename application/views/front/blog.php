<div class="breadcrumb-section image-bg">
   <div class="overlay"></div>
   <div class="breadcrumb-content container">
     <h1>Our Blog</h1>
     <ol class="breadcrumb">
       <li><a href="">Home</a></li>
       <li class="active">Blog</li>
     </ol>
   </div>
</div>
<div class="blog-section section-padding">
   <div class="container">
      <div class="section-title text-center">
         <h1>Blog & News</h1>
         <h2>Keep Updated about US</h2>
      </div>
      <div class="blog-content">
         <div class="row">
            <?php foreach ($blogs->result() as $blog): ?>
            <div class="col-md-4 col-sm-6">
               <div class="blog-post">
                  <div class="entry-header">
                     <div class="blog-image">
                        <div class="entry-thumbnail">
                           <a href="web/blog_detail/<?php echo $blog->slug ?>">
                              <img class="img-responsive" src="#" >
                           </a>
                        </div>
                        <div class="time">
                           <h2><?php echo tgl_split_indo($blog->created_at, 1) ?> <span><?php echo tgl_split_indo($blog->created_at, 2) ?></span></h2>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="entry-post">
                     <div class="entry-title">
                        <h4><a href="web/blog_detail/<?php echo $blog->slug ?>"><?php echo $blog->judul ?></a></h4>
                     </div>
                     <div class="post-content">
                        <div class="entry-summary">
                           <?php echo substr($blog->isi, 0,150) ?>..
                           <div class="entry-meta">
                              <ul class="list-inline">
                                 <li><a href="#"><i class="fa fa-user"></i><?php echo get_data('a_user','id_user',$blog->id_user,'nama_lengkap') ?></a></li>
                              <li><a href="#"><i class="fa fa-calendar"></i>Date : <?php echo tgl_indo($blog->created_at) ?></a></li>                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php endforeach ?>
         </div>
      </div>
      <!-- <div class="pagination-section text-center">
         <ul class="pagination">
            <li>
               <a href="#" aria-label="Previous">
               <span aria-hidden="true">&laquo;</span>
               </a>
            </li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
               <a href="#" aria-label="Next">
               <span aria-hidden="true">&raquo;</span>
               </a>
            </li>
         </ul>
      </div> -->
   </div>
</div>