<div class="breadcrumb-section image-bg">
      <div class="overlay"></div>
      <div class="breadcrumb-content container">
        <h1>Contact Us</h1>
        <ol class="breadcrumb">
          <li><a href="">Home</a></li>
          <li class="active">Contact</li>
        </ol>
      </div>
    </div>
    <div class="contact-form-section section-padding">
      <div class="container">
        <div class="section-title text-center">
          <div class="container">
            <h1>Contact Us</h1>
            <h2>Drop Your Message</h2>
          </div>
        </div>
        <div class="contact-info">
          <form
            id="contact-form"
            class="contact-form"
            name="contact-form"
            method="post"
            action="#"
          >
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  <input
                    type="text"
                    class="form-control control-1"
                    required="required"
                    placeholder="Name"
                  />
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <i class="fa fa-th-list" aria-hidden="true"></i>
                  <input
                    type="text"
                    class="form-control"
                    required="required"
                    placeholder="Subject"
                  />
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                  <input
                    type="email"
                    class="form-control"
                    required="required"
                    placeholder="E-mail"
                  />
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <i class="fa fa-comments" aria-hidden="true"></i>
                  <textarea
                    name="message"
                    id="message"
                    required="required"
                    class="form-control"
                    rows="7"
                    placeholder="Message"
                  ></textarea>
                </div>
              </div>
            </div>
            <div class="submit-button">
              <button type="submit" class="btn btn-primary">Send Now</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div id="gmap">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253840.65295119272!2d106.68942926215716!3d-6.229386698419925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta%2C%20Daerah%20Khusus%20Ibukota%20Jakarta!5e0!3m2!1sid!2sid!4v1640075644859!5m2!1sid!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>