<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="Grid Bootstrap" />
    <meta name="description" content="" />
    <title>PT Gentala Bumi Nusantara | GENTARA</title>
    <base href="<?php echo base_url() ?>">

    <link href="front/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="front/css/font-awesome.min.css" />
    <link rel="stylesheet" href="front/css/animate.css" />
    <link rel="stylesheet" href="front/css/magnific-popup.css" />
    <link rel="stylesheet" href="front/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="front/css/owl.carousel.css" />
    <link rel="stylesheet" href="front/css/main.css" />
    <link rel="stylesheet" href="front/css/responsive.css" />

    <link rel="icon" href="image/logo_gentara.png" />
    

  </head>
  <body>

    <?php $this->load->view('front/header'); ?>

    <?php $this->load->view($konten); ?>
    
    <?php $this->load->view('front/footer'); ?>

  </body>
</html>
