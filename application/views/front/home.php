<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <?php 
    $no = 1;
    foreach ($slider->result() as $slide): ?>
    <div class="item <?php echo ($no == 1) ? 'active' : '' ?>">
      <img src="image/slide/<?php echo $slide->image ?>" alt="image" style="width:100%;">
    </div>
    <?php $no++; endforeach ?>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

    <div class="features-section section-padding">
      <div class="container">
        <div class="section-title text-center">
          <h1>NILAI GENTARA</h1>
          <h2>“Prinsip kerja yang menjadi ruh dalam melayani”</h2>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-road" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Integritas</h4>
                <p>
                  Memilih untuk melakukan hal benar
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-certificate" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Kerja Sama</h4>
                <p>
                  Tumbuh besar karena soliditas tim.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Haus Ilmu</h4>
                <p>
                  Selalu belajar dan bersahabat dengan tantangan.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-hand-lizard-o" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Layanan Terbaik</h4>
                <p>
                  Membantu anda adalah tujuan kami.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-universal-access" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Aktualisasi Ide</h4>
                <p>
                  Berani bertanggung jawab dalam mengeksekusi pikiran dan ide baru.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="features">
              <div class="feature-icon">
                <i class="fa fa-sitemap" aria-hidden="true"></i>
              </div>
              <div class="feature-text">
                <h4>Semangat Pebaikan</h4>
                <p>
                  Hadir memberi manfaat untuk kebaikan sekitar.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="service-section section-padding">
      <div class="container">
        <div class="section-title text-center">
          <h1>Our Services</h1>
          <h2>We Offer Our Clients</h2>
        </div>
        <div class="service-content">
          <div class="services">
            <div class="row">

              <?php foreach ($services->result() as $rw): ?>
              <div class="col-sm-6 col-md-4">
                <div class="service crane-lifting image-bg">
                  <div class="overlay"></div>
                  <div class="image-box">
                    <img
                      class="img-responsive"
                      src="image/service/<?php echo $rw->icon ?>"
                      alt="Image"
                    />
                  </div>
                  <div class="box-title">
                    <h4><?php echo $rw->judul ?></h4>
                    <p>
                      <?php echo $rw->deskripsi?>
                    </p>

                  </div>
                </div>
              </div>
              <?php endforeach ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="team-section section-padding">
      <div class="container">
        <div class="section-title text-center">
          <h1>OUR Team</h1>
          <h2>We have Experienced Members</h2>
        </div>
        <div class="team-members">
          <div class="row">
            <div class="col-md-12">
              <img src="image/team.png" style="width: 100%;">
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="portfolio-section">
      <div class="portfolio-title image-bg">
        <div class="overlay"></div>
        <div class="section-title">
          <div class="container">
            <h1>The Projects</h1>
            <h2>We already completed</h2>
          </div>
        </div>
      </div>
      <div class="recent-projects">
        <div id="recent-projects">
          <div class="project-content">
            <div class="project-title">
              <h3><a href="project-details.html">Nam liber tempor cum</a></h3>
              <span>Clients: Dreamer</span>
            </div>
            <div class="project-item">
              <a href="images/project/1.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/1.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Duis autem vel eum iriure dolor in hendrerit in vulputate velit
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3><a href="project-details.html">Lorem ipsum dolor sit</a></h3>
              <span>Clients: Asthetic</span>
            </div>
            <div class="project-item">
              <a href="images/project/2.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/2.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Nam liber tempor cum soluta nobis eleifend option congue nihil
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3>
                <a href="project-details.html">Legentis in iis qui facit</a>
              </h3>
              <span>Clients: Martin</span>
            </div>
            <div class="project-item">
              <a href="images/project/3.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/3.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Typi non habent claritatem insitam; est usus legentis in iis qui
                facit
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3><a href="project-details.html">Duis autem vel eum </a></h3>
              <span>Clients: Marbel</span>
            </div>
            <div class="project-item">
              <a href="images/project/4.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/4.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Mirum est notare quam littera gothica, quam nunc putamus parum
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3><a href="project-details.html">Lorem ipsum dolor sit</a></h3>
              <span>Clients: Asthetic</span>
            </div>
            <div class="project-item">
              <a href="images/project/2.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/5.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Nam liber tempor cum soluta nobis eleifend option congue nihil
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3>
                <a href="project-details.html">Legentis in iis qui facit</a>
              </h3>
              <span>Clients: Martin</span>
            </div>
            <div class="project-item">
              <a href="images/project/3.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/6.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Typi non habent claritatem insitam; est usus legentis in iis qui
                facit
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3>
                <a href="project-details.html">Legentis in iis qui facit</a>
              </h3>
              <span>Clients: Martin</span>
            </div>
            <div class="project-item">
              <a href="images/project/3.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/7.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Typi non habent claritatem insitam; est usus legentis in iis qui
                facit
              </p>
            </div>
          </div>
          <div class="project-content">
            <div class="project-title">
              <h3><a href="project-details.html">Duis autem vel eum </a></h3>
              <span>Clients: Marbel</span>
            </div>
            <div class="project-item">
              <a href="images/project/4.jpg" class="image-link"
                ><img class="img-responsive" src="front/images/project/8.jpg" alt=""
              /></a>
            </div>
            <div class="project-info">
              <p>
                Mirum est notare quam littera gothica, quam nunc putamus parum
              </p>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    
    
