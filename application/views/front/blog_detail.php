<div class="breadcrumb-section image-bg ">
   <div class="overlay"></div>
   <div class="breadcrumb-content container">
      <h1>Blog Details</h1>
      <ol class="breadcrumb">
         <li><a href="">Home</a></li>
         <li class="active">Blog</li>
      </ol>
   </div>
</div>
<?php 
$slug = $this->uri->segment(3);
$this->db->like('slug', $slug, 'AFTER');
$blog = $this->db->get('artikel')->row();

 ?>
<div class="blog-details">
   <div class="container">
      <div class="blog-content">
         <div class="blog-image">
            <div class="entry-thumbnail">
               <img class="img-responsive" src="#">
            </div>
            <div class="time">
               <h2><?php echo tgl_split_indo($blog->created_at, 1) ?> <span><?php echo tgl_split_indo($blog->created_at, 2) ?></span></h2>
            </div>
         </div>
         <div class="entry-title">
            <h3><?php echo $blog->judul ?></h3>
         </div>
         <div class="entry-meta">
            <ul class="list-inline">
               <li><a href="#"><i class="fa fa-user"></i><?php echo get_data('a_user','id_user',$blog->id_user,'nama_lengkap') ?></a></li>
               <li><a href="#"><i class="fa fa-calendar"></i>Date : <?php echo tgl_indo($blog->created_at) ?></a></li>
            </ul>
         </div>
         <div class="post-content">
            <?php echo $blog->isi ?>
         </div>
         <!-- <div class="share-social">
            <ul class="social">
               <li>Share:</li>
               <li><a href="#"><i class="fa fa-rss"></i></a></li>
               <li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
               <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
               <li><a href="#"><i class="fa fa-flickr"></i></a></li>
               <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
            </ul>
         </div>
         <div class="post-author">
            <div class="author-image">
               <img class="img-responsive" src="images/blog/user.jpg" alt="Image">
            </div>
            <div class="post-author-info">
               <h4>Mark Doe</h4>
               <h5>Senior Consultant</h5>
               <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi archo beatae to vitae dicta sunt explicabo.</p>
            </div>
         </div>
         <div class="comments-area">
            <h4>3 Comments:</h4>
            <div class="post-comment">
               <div class="post-nfo">
                  <div class="author-image">
                     <img class="img-responsive" src="images/blog/user-1.jpg" alt="Image">
                  </div>
                  <div class="commenter-info">
                     <h5>Jhon Doe</h5>
                     <p class="date">Posted on Aug 9, 2016</p>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco off a laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                     <a href="#"> <i class="fa fa-share" aria-hidden="true"></i> Replay</a>
                  </div>
               </div>
            </div>
            <div class="post-comment">
               <div class="post-nfo">
                  <div class="author-image">
                     <img class="img-responsive" src="images/blog/user.jpg" alt="Image">
                  </div>
                  <div class="commenter-info">
                     <h5>Adam Marsal</h5>
                     <p class="date">Posted on Aug 9, 2016</p>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim too veniam, quis nos to to it xercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
                     <a href="#"> <i class="fa fa-share" aria-hidden="true"></i> Replay</a>
                  </div>
               </div>
            </div>
            <div class="post-comment">
               <div class="post-nfo">
                  <div class="author-image">
                     <img class="img-responsive" src="images/blog/user3.jpg" alt="Image">
                  </div>
                  <div class="commenter-info">
                     <h5>Maria Liza</h5>
                     <p class="date">Posted on Aug 9, 2016</p>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim too veniam, quis nos to to it xercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
                     <a href="#"> <i class="fa fa-share" aria-hidden="true"></i> Replay</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="replay-box">
            <h4>Leave A Comment</h4>
            <form id="contact-form" class="contact-form" name="contact-form" method="post" action="#">
               <div class="row">
                  <div class="col-sm-4">
                     <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control control-1" required="required" placeholder="Your Name">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" required="required" placeholder="E-mail Address">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <input type="text" class="form-control" required="required" placeholder="Web Address">
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                        <textarea name="message" id="message" required="required" class="form-control" rows="7" placeholder="Your Comments"></textarea>
                     </div>
                  </div>
               </div>
               <div class="submit-button">
                  <button type="submit" class="btn btn-primary">Submit Your Comment</button>
               </div>
            </form>
         </div> -->
      </div>
   </div>
</div>