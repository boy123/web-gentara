<div class="breadcrumb-section image-bg">
  <div class="overlay"></div>
  <div class="breadcrumb-content container">
    <h1>About Us</h1>
    <ol class="breadcrumb">
      <li><a href="">Home</a></li>
      <li class="active">Services</li>
    </ol>
  </div>
</div>

<div class="service-section section-padding">
      <div class="container">
        <div class="section-title text-center">
          <h1>Our Services</h1>
          <h2>We Offer Our Clients</h2>
        </div>
        <div class="service-content">
          <div class="services">
            <div class="row">

              <?php foreach ($services->result() as $rw): ?>
              <div class="col-sm-6 col-md-4">
                <div class="service crane-lifting image-bg">
                  <div class="overlay"></div>
                  <div class="image-box">
                    <img
                      class="img-responsive"
                      src="image/service/<?php echo $rw->icon ?>"
                      alt="Image"
                    />
                  </div>
                  <div class="box-title">
                    <h4><?php echo $rw->judul ?></h4>
                    <p>
                      <?php echo $rw->deskripsi?>
                    </p>

                  </div>
                </div>
              </div>
              <?php endforeach ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
