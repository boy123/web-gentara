
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Kategori <?php echo form_error('id_kategori') ?></label>
            <select name="id_kategori" class="form-control">
                <option value="<?php echo $id_kategori ?>"><?php echo get_data('kategori','id_kategori',$id_kategori,'kategori') ?></option>
                <?php foreach ($this->db->get('kategori')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_kategori ?>"><?php echo $value->kategori ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
	    <div class="form-group">
            <label for="isi">Isi <?php echo form_error('isi') ?></label>
            <textarea class="form-control editor" rows="3" name="isi" id="isi" placeholder="Isi"><?php echo $isi; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="keyword">Keyword <?php echo form_error('keyword') ?><span style="color: red;">*) Pisahkan dengan koma.</span><br></label>
            <textarea class="form-control" rows="3" name="keyword" id="keyword" placeholder="Keyword"><?php echo $keyword; ?></textarea>
        </div>
	    
	    <input type="hidden" name="id_artikel" value="<?php echo $id_artikel; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('artikel') ?>" class="btn btn-default">Cancel</a>
	</form>
   