
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Icon </label>
            <input type="file" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" />
            <input type="hidden" name="icon_old" value="<?php echo $icon ?>">
            <p>
                <?php if ($icon != ''): ?>
                    *) Gambar sebelumnya <br>
                    <img src="image/service/<?php echo $icon ?>" style="width: 50px;">
                <?php endif ?>
                
            </p>
        </div>
	    <div class="form-group">
            <label for="deskripsi">Deskripsi <?php echo form_error('deskripsi') ?></label>
            <textarea class="form-control" rows="3" name="deskripsi" id="deskripsi" placeholder="Deskripsi"><?php echo $deskripsi; ?></textarea>
        </div>
	    <input type="hidden" name="id_services" value="<?php echo $id_services; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('services') ?>" class="btn btn-default">Cancel</a>
	</form>
   